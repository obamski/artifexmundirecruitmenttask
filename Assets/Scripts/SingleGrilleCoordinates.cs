﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleGrilleCoordinates : MonoBehaviour
{

    [SerializeField]
    private int xCoordinate;
    [SerializeField]
    private int yCoordinate;

    public int XCoordinate
    {
        get { return xCoordinate; }
        set { xCoordinate = value; }
    }

    public int YCoordinate
    {
        get { return yCoordinate; }
        set { yCoordinate = value; }
    }
}
