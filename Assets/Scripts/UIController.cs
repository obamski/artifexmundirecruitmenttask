﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    [SerializeField]
    private GameObject mainMenuPanel;
    [SerializeField]
    private List<GameObject> allGrills;
    [SerializeField]
    private Text bestScoreText;

    public void StartGame()
    {
        StartCoroutine(StartGameWithAnimation());
    }
    public void ResetScore()
    {
        PlayerPrefs.SetFloat(MatchesController.BEST_SCORE_PREFS, 0);
        bestScoreText.text = MatchesController.BEST_SCORE_TEXT_PREFIX + "0";
    }

    IEnumerator StartGameWithAnimation()
    {
        mainMenuPanel.GetComponent<Animator>().SetBool("GameStart", true);
        yield return new WaitForSeconds(0.5f);
        foreach (GameObject grille in allGrills)
        {

            grille.GetComponentInChildren<PebbleController>().gameObject.GetComponent<Animator>().Play("pebbleSpawn", -1);
            grille.GetComponentInChildren<PebbleController>().gameObject.GetComponent<Animator>().SetBool("canSpawn", false);

        }
        MatchesController.points = 0;
        mainMenuPanel.SetActive(false);
    }
}



