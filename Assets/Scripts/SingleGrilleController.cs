﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class SingleGrilleController : MonoBehaviour
{
    #region Delegates
    public delegate void SwipeAction();
    public static event SwipeAction OnSwipeFinih;
    #endregion
    #region Public variables
    public List<GameObject> possibleMatchesMiddle;
    public List<GameObject> possibleMatchesRight;
    public List<GameObject> possibleMatchesLeft;
    public bool isMatched = false;
    #endregion
    #region Serializable variables

    [SerializeField]
    private GameObject leftNeighbourGrille;
    [SerializeField]
    private GameObject rightNeighbourGrille;
    [SerializeField]
    private GameObject upNeighbourGrille;
    [SerializeField]
    private GameObject downNeighbourGrille;
    [SerializeField]
    private GameObject pebblePrefab;
    [SerializeField]
    private List<Sprite> pebbleColorSprites = new List<Sprite>();

    #endregion
    #region Private variables with properties
    private int randomColorIndex;

    private int currentColorID;
    public int CurrentColorID
    {
        set { currentColorID = value; }
        get { return currentColorID; }
    }

    private GameObject pebbleChild;
    public GameObject PebbleChild
    {
        get { return pebbleChild; }
    }

    #endregion



    #region Spawning methods
    public void SpawnPebbleAtGrillePosition()
    {
        Vector3 grillPosition = new Vector3(transform.position.x, transform.position.y, (transform.position.z + 2));
        pebbleChild = Instantiate(pebblePrefab, grillPosition, transform.rotation);
        pebbleChild.transform.parent = gameObject.transform;
        AssignRandomColorToSpawnedPebble(pebbleChild);
    }


    private void AssignRandomColorToSpawnedPebble(GameObject pebbleBeingSpawned)
    {
        randomColorIndex = GetRandomColorDraw();
        currentColorID = randomColorIndex;
        pebbleBeingSpawned.GetComponent<SpriteRenderer>().sprite = pebbleColorSprites[randomColorIndex];
        pebbleBeingSpawned.GetComponent<PebbleController>().PebbleColorID = randomColorIndex;

    }

    int GetRandomColorDraw()
    {
        return Random.Range(0, pebbleColorSprites.Count);
    }
    #endregion
    #region After swpipe methods
    public void PebbleSwipe(int swipeID)
    {
        currentColorID = pebbleChild.GetComponent<PebbleController>().PebbleColorID;
        MatchesController.playerMatchedPebbles = false;
        switch (swipeID)
        {
            case ((int)Swipes.RIGHT):
                if (rightNeighbourGrille != null)
                {
                    MovePebbleAfterSwipe(rightNeighbourGrille);
                }
                 break;

            case ((int)Swipes.LEFT):
                if (leftNeighbourGrille != null && leftNeighbourGrille.GetComponentInChildren<PebbleController>() != null)
                {
                    MovePebbleAfterSwipe(leftNeighbourGrille);
                }
                break;

            case ((int)Swipes.UP):
                if (upNeighbourGrille != null)
                {
                    MovePebbleAfterSwipe(upNeighbourGrille);
                }
                break;

            case ((int)Swipes.DOWN):
                if (downNeighbourGrille != null)
                {
                    MovePebbleAfterSwipe(downNeighbourGrille);
                }
                break;
        }
    }


    void MovePebbleAfterSwipe(GameObject neighbourGrille)
    {
        int neighbourPebbleId = neighbourGrille.GetComponentInChildren<PebbleController>().PebbleColorID;
        int pastNeighbourID = neighbourPebbleId;
        neighbourGrille.GetComponentInChildren<PebbleController>().ChangePebbleColor(pebbleColorSprites[currentColorID], currentColorID);
        neighbourGrille.GetComponent<SingleGrilleController>().currentColorID = currentColorID;
        int pastID = currentColorID;
        pebbleChild.GetComponent<PebbleController>().ChangePebbleColor(pebbleColorSprites[neighbourPebbleId], neighbourPebbleId);
        currentColorID = neighbourPebbleId;
        if (OnSwipeFinih != null)
            OnSwipeFinih.Invoke();

        if (!MatchesController.playerMatchedPebbles)
        {
            StartCoroutine(UndoPlayersMove(neighbourGrille, neighbourPebbleId, pastID, pastNeighbourID));
        }
    }

    IEnumerator UndoPlayersMove(GameObject neighbourGrille, int neighbourPebbleId, int pastID, int pastNeighbourID)
    {
        yield return new WaitForSeconds(0.5f);
        pebbleChild.GetComponentInChildren<PebbleController>().ChangePebbleColor(pebbleColorSprites[pastID], pastID);
        neighbourGrille.GetComponent<SingleGrilleController>().currentColorID = pastNeighbourID;
        neighbourGrille.GetComponentInChildren<PebbleController>().ChangePebbleColor(pebbleColorSprites[pastNeighbourID], pastNeighbourID);
        currentColorID = pastID;

    }
    #endregion


}
