﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchesController : MonoBehaviour
{
    #region Constant variables
    public const string BEST_SCORE_PREFS = "BEST_SCORE";
    public const string BEST_SCORE_TEXT_PREFIX = "BEST SCORE: ";
    private const string PEBBLE_TAG = "pebble";
    private const string PEBBLE_ANIMATOR_START = "canSpawn";
    private const string PEBBLE_ANIMATOR_DEATH = "PebbleDeath";
    private const int PEBBLE_DEATH_ANIMATION_LAYER = -1;
    private const float VERTICAL_CHECK_DELAY = 0.3f;
    private const float SHORT_SPAWN_DELAY = 1.5f;
    private const float POINTS_FOR_MATCHES = 0.5f;
    #endregion
    #region Public variables

    public delegate void PebblesAction();
    public static event PebblesAction OnPebblesChange;
    public static bool playerMatchedPebbles;
    public static float points = 0;
    public bool wereMatches;

    #endregion
    #region Serializable variables
    #region Grills rows

    [Header("GRILLS ROWS")]
    [SerializeField]
    protected List<GameObject> firstRowGrills;

    public List<GameObject> FirstRowGrills
    {
        get { return firstRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> secondRowGrills;
    public List<GameObject> SecondRowGrills
    {
        get { return secondRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> thirdRowGrills;
    public List<GameObject> ThirdRowGrills
    {
        get { return thirdRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> fourthRowGrills;
    public List<GameObject> FourthRowGrills
    {
        get { return fourthRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> fifthRowGrills;
    public List<GameObject> FifthRowGrills
    {
        get { return fifthRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> sixthRowGrills;
    public List<GameObject> SixthRowGrills
    {
        get { return sixthRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> seventhRowGrills;
    public List<GameObject> SeventhRowGrills
    {
        get { return seventhRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> eightRowGrills;
    public List<GameObject> EightRowGrills
    {
        get { return eightRowGrills; }
    }
    [SerializeField]
    protected List<GameObject> ninthRowGrills;
    public List<GameObject> NinthRowGrills
    {
        get { return ninthRowGrills; }
    }



    #endregion
    #region Lists of columns
    [Header("GRILLS COLUMNS")]

    [SerializeField]
    protected List<GameObject> firstColumnGrills;
    [SerializeField]
    protected List<GameObject> secondColumnGrills;
    [SerializeField]
    protected List<GameObject> thirdColumnGrills;
    [SerializeField]
    protected List<GameObject> fourthColumnGrills;
    [SerializeField]
    protected List<GameObject> fifthColumnGrills;
    [SerializeField]
    protected List<GameObject> sixthColumnGrills;
    [SerializeField]
    protected List<GameObject> seventhColumnGrills;
    [SerializeField]
    protected List<GameObject> eightColumnGrills;
    [SerializeField]
    protected List<GameObject> ninthColumnGrills;
    #endregion
    [SerializeField]
    private Text pointsText;
    [SerializeField]
    private Text bestScoreText;
    [SerializeField]
    private List<GameObject> allGrills;
    [SerializeField]
    protected List<Sprite> pebbleSprites = new List<Sprite>();
    #endregion
    #region Private variables

    private bool isFirstCheck = false;
    private int colorCounterHorizontal;
    private int currentIndexHorizontal;
    int colorCounterVertical;
    int currentIndexVertical;
    private int firstPairColumn = -1;
    private int firstPairRow = -1;
    private int currentPebbleID = 0;
    private float speedOfBlockFalling = 0.001f;
    private float blockFallDelay = 0.1f;
   
    #endregion


    void OnEnable()
    {

        SingleGrilleController.OnSwipeFinih += CheckMatches;
    }


    void OnDisable()
    {
        SingleGrilleController.OnSwipeFinih -= CheckMatches;
    }
    public void CheckMatches()
    {
        #region Check matches horizontal
        CheckForMatchesHorizontal(ninthRowGrills, 9);
        CheckForMatchesHorizontal(eightRowGrills, 8);
        CheckForMatchesHorizontal(seventhRowGrills, 7);
        CheckForMatchesHorizontal(sixthRowGrills, 6);
        CheckForMatchesHorizontal(fifthRowGrills, 5);
        CheckForMatchesHorizontal(fourthRowGrills, 4);
        CheckForMatchesHorizontal(thirdRowGrills, 3);
        CheckForMatchesHorizontal(secondRowGrills, 2);
        CheckForMatchesHorizontal(firstRowGrills, 1);
        #endregion
        #region Check matches vertical

        StartCoroutine(CheckForMatchesVertical(ninthColumnGrills, 9));
        StartCoroutine(CheckForMatchesVertical(eightColumnGrills, 8));
        StartCoroutine(CheckForMatchesVertical(seventhColumnGrills, 7));
        StartCoroutine(CheckForMatchesVertical(sixthColumnGrills, 6));
        StartCoroutine(CheckForMatchesVertical(fifthColumnGrills, 5));
        StartCoroutine(CheckForMatchesVertical(fourthColumnGrills, 4));
        StartCoroutine(CheckForMatchesVertical(thirdColumnGrills, 3));
        StartCoroutine(CheckForMatchesVertical(secondColumnGrills, 2));
        StartCoroutine(CheckForMatchesVertical(firstColumnGrills, 1));

        #endregion

        if (wereMatches)
        {
            wereMatches = false;
            if (!isFirstCheck)
                StartCoroutine(CheckMatchesAfterDelay());
            else
                CheckMatches();
        }
    }

    IEnumerator CheckMatchesAfterDelay()
    {
        yield return new WaitForSeconds(0.5f);
        CheckMatches();
    }


    #region Level starting methods
    void Start()
    {
        
        AssignStartingValues();
        SpawnPebbles();
        CheckMatches();
        if (!wereMatches)
        {
            StartCoroutine(ShowPebblesAfterCheck());
        }
    }

    private void AssignStartingValues()
    {
        bestScoreText.text = BEST_SCORE_TEXT_PREFIX + PlayerPrefs.GetFloat(BEST_SCORE_PREFS);

        isFirstCheck = true;
        wereMatches = false;
        speedOfBlockFalling = 0;
        blockFallDelay = 0;
    }

    private void SpawnPebbles()
    {
        foreach (GameObject grille in allGrills)
        {
            grille.GetComponentInChildren<SingleGrilleController>().SpawnPebbleAtGrillePosition();
        }
    }


    IEnumerator ShowPebblesAfterCheck()
    {
        yield return new WaitForSeconds(SHORT_SPAWN_DELAY);
        foreach (var pebble in GameObject.FindGameObjectsWithTag(PEBBLE_TAG))
        {
            pebble.GetComponent<Animator>().SetBool(PEBBLE_ANIMATOR_START, true);
        }
        speedOfBlockFalling = 0.0001f;
        blockFallDelay = 0.001f;

        if (OnPebblesChange != null)
        {
            OnPebblesChange();
        }
        isFirstCheck = false;
    }

    #endregion
    #region Horizontal matches methods

    public void CheckForMatchesHorizontal(List<GameObject> currentRow, int rowNumber)
    {
        colorCounterHorizontal = 0;
        currentIndexHorizontal = 0;
        foreach (GameObject grille in currentRow)
        {
            GetCurrentPebbleID(grille);

            if (PebbleHasRightNeighbour(currentIndexHorizontal, currentRow))
            {
                CheckIfPebbleMatchingHorizontal(currentRow, rowNumber);
            }

            else
            {
                CheckLastPebbleInRowHorizontal(currentRow, rowNumber);
            }

            currentIndexHorizontal++;
        }
    }

    void CheckLastPebbleInRowHorizontal(List<GameObject> currentRow, int rowNumber)
    {
        if (LastPebbleInRowCurrentlyActive(currentRow))
        {
            if (LastPebbleInRowIsAMatch(currentRow))
            {
                ColorsWereMatching(colorCounterHorizontal, currentRow, currentIndexHorizontal, rowNumber);
            }
        }

        colorCounterHorizontal = 0;
    }

    void CheckIfPebbleMatchingHorizontal(List<GameObject> currentRow, int rowNumber)
    {
        if (PebbleRightNeighbourHasSameColor(currentIndexHorizontal, currentRow))
        {
            colorCounterHorizontal++;
        }
        else
        {
            if (colorCounterHorizontal >= 2)
            {
                ColorsWereMatching(colorCounterHorizontal, currentRow, currentIndexHorizontal, rowNumber);
            }

            colorCounterHorizontal = 0;
        }

    }

    void ColorsWereMatching(int colorCounter, List<GameObject> currentRow, int currentIndex, int rowNumber)
    {
        int bufor = currentIndex;
        wereMatches = true;
        for (int z = 0; z <= colorCounter; z++)
        {
            if (PebbleCurrentlyActive(currentRow[bufor]))
            {
                if (!isFirstCheck)
                {
                    currentRow[bufor].GetComponent<Animator>().Play(PEBBLE_ANIMATOR_DEATH, PEBBLE_DEATH_ANIMATION_LAYER);
                }
                StartCoroutine(MakeHorizontalPebblesFall(bufor, rowNumber, currentRow[bufor].GetComponentInChildren<PebbleController>().gameObject));
                currentRow[bufor].GetComponentInChildren<PebbleController>().gameObject.SetActive(false);
                bufor--;
                IncrementPoints();
            }
        }
    }

    IEnumerator MakeHorizontalPebblesFall(int columnNumber, int rowNumber, GameObject basePebble)
    {
        playerMatchedPebbles = true;
        yield return new WaitForSeconds(blockFallDelay);
        if (rowNumber != 1)
        {
            Sprite higherPebbleSprite = GetPebbleSprite(columnNumber, rowNumber - 1);
            int higherPebbleID = GetPebbleID(columnNumber, rowNumber - 1);
            GameObject higherPebble = GetPebbleFromCoordinates(columnNumber, rowNumber - 1);
            if (higherPebble != null && !isFirstCheck)
            {
                higherPebble.GetComponent<SpriteRenderer>().enabled = false;
            }
            basePebble.GetComponent<PebbleController>().ChangePebbleColor(higherPebbleSprite, higherPebbleID);
            basePebble.SetActive(true);

            for (int i = (rowNumber - 1); i >= 2; i--)
            {
                MovePebblesDown(columnNumber, i);
                yield return new WaitForSeconds(0.1f);
            }
        }
        StartCoroutine(SpawnNewPebbles(columnNumber));
        CheckForGaps();
    }
    #endregion

    #region Vertical matches methods
    public IEnumerator CheckForMatchesVertical(List<GameObject> currentColumn, int rowNumber)
    {
        yield return new WaitForSeconds(VERTICAL_CHECK_DELAY);
         colorCounterVertical = 0;
         currentIndexVertical = 0;
        foreach (GameObject grille in currentColumn)
        {
            if (grille.GetComponentInChildren<PebbleController>() != null)
            {
                currentPebbleID = grille.GetComponentInChildren<PebbleController>().PebbleColorID;
            }

            if (currentIndexVertical < 8 && currentColumn[currentIndexVertical + 1].GetComponentInChildren<PebbleController>() != null)
            {
                CheckVerticalColorsMatch(currentColumn, rowNumber);
            }

            else
            {
                CheckLastMatchInColumn(currentColumn, rowNumber);

            }
            currentIndexVertical++;
        }
    }


    void CheckLastMatchInColumn(List<GameObject> currentColumn, int rowNumber)
    {
        if (currentColumn[currentIndexVertical - 1].GetComponentInChildren<PebbleController>() != null)
        {
            if (currentPebbleID == currentColumn[currentIndexVertical - 1].GetComponentInChildren<PebbleController>().PebbleColorID && colorCounterVertical >= 2)
            {
                int bufor = currentIndexVertical;
                wereMatches = true;
                for (int z = 0; z <= colorCounterVertical; z++)
                {
                    if (currentColumn[bufor].GetComponentInChildren<PebbleController>() != null)
                        StartCoroutine(MakePebblesFallVertical(bufor, rowNumber, currentColumn[bufor].gameObject));
                    IncrementPoints();
                    bufor--;
                }
            }
        }
        colorCounterVertical = 0;
    }


    void CheckVerticalColorsMatch(List<GameObject> currentColumn, int rowNumber)
    {
        if (currentPebbleID == currentColumn[currentIndexVertical + 1].GetComponentInChildren<PebbleController>().PebbleColorID)
        {
            colorCounterVertical++;
        }
        else
        {
            if (colorCounterVertical >= 2)
            {
                int bufor = currentIndexVertical;
                wereMatches = true;
                for (int z = 0; z <= colorCounterVertical; z++)
                {
                    if (currentColumn[bufor].GetComponentInChildren<PebbleController>() != null)
                    {
                        StartCoroutine(MakePebblesFallVertical(bufor, rowNumber, currentColumn[bufor].gameObject));
                        bufor--;
                        IncrementPoints();

                    }
                }
            }
            colorCounterVertical = 0;
        }
    }


    IEnumerator MakePebblesFallVertical(int columnNumber, int rowNumber, GameObject basePebble)
    {
        yield return new WaitForSeconds(0.01f);
        int newBaseID = Random.Range(0, 5);
        basePebble.GetComponentInChildren<PebbleController>().ChangePebbleColor(pebbleSprites[newBaseID], newBaseID);
        basePebble.GetComponent<SingleGrilleController>().CurrentColorID = newBaseID;
        StartCoroutine(SpawnNewPebbles(columnNumber));
        CheckForGaps();

    }

    #endregion

    IEnumerator SpawnNewPebbles(int columnIndex)
    {

        yield return new WaitForSeconds(blockFallDelay);

        if (firstRowGrills[columnIndex].GetComponentInChildren<PebbleController>() == null)
        {
            foreach (Transform child in firstRowGrills[columnIndex].transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        GameObject pebbleToSpawn = firstRowGrills[columnIndex].GetComponentInChildren<PebbleController>().gameObject;
        pebbleToSpawn.SetActive(false);
        int randomColorIndex = Random.Range(0, 5);
        pebbleToSpawn.GetComponent<SpriteRenderer>().enabled = true;
        pebbleToSpawn.GetComponentInChildren<PebbleController>().ChangePebbleColor(pebbleSprites[randomColorIndex], randomColorIndex);

        pebbleToSpawn.SetActive(true);
        CheckMatches();
        if (!wereMatches)
        {
            if (OnPebblesChange != null)
                OnPebblesChange.Invoke();
        }
    }


    private void MovePebblesDown(int columnNumber, int rowNumber)
    {
        GameObject oneHigherPebble = GetPebbleFromCoordinates(columnNumber, rowNumber);
        if (oneHigherPebble != null)
            oneHigherPebble.GetComponent<SpriteRenderer>().enabled = true;
        GameObject twoHigherPebble = GetPebbleFromCoordinates(columnNumber, rowNumber - 1);
        Sprite twoHigherPebbleSprite = GetPebbleSprite(columnNumber, rowNumber - 1);
        int twoHigherPebbleID = GetPebbleID(columnNumber, rowNumber - 1);
        if (oneHigherPebble != null)
            oneHigherPebble.GetComponent<PebbleController>().ChangePebbleColor(twoHigherPebbleSprite, twoHigherPebbleID);
        if (twoHigherPebble != null && !isFirstCheck)
            twoHigherPebble.GetComponent<SpriteRenderer>().enabled = false;
    }

    void CheckForGaps()
    {
        StartCoroutine(CheckForGapsInRow(ninthRowGrills, 9));
        StartCoroutine(CheckForGapsInRow(eightRowGrills, 8));
        StartCoroutine(CheckForGapsInRow(seventhRowGrills, 7));
        StartCoroutine(CheckForGapsInRow(sixthRowGrills, 6));
        StartCoroutine(CheckForGapsInRow(fifthRowGrills, 5));
        StartCoroutine(CheckForGapsInRow(fourthRowGrills, 4));
        StartCoroutine(CheckForGapsInRow(thirdRowGrills, 3));
        StartCoroutine(CheckForGapsInRow(secondRowGrills, 2));
        StartCoroutine(CheckForGapsInRow(firstRowGrills, 1));
    }


    IEnumerator CheckForGapsInRow(List<GameObject> currentRows, int rowIndex)
    {
        yield return new WaitForSeconds(0.0001f);
        int currentColumn = 0;
        foreach (GameObject grille in currentRows)
        {
            currentColumn++;
            if (grille.GetComponentInChildren<PebbleController>() != null)
            {
                GameObject pebble = grille.GetComponentInChildren<PebbleController>().gameObject;

                if (pebble.GetComponent<SpriteRenderer>().sprite == null)
                {
                    pebble.GetComponent<SpriteRenderer>().sprite = pebbleSprites[Random.Range(0, 5)];
                }

            }

        }
    }


    void GetCurrentPebbleID(GameObject grilleObject)
    {
        if (PebbleCurrentlyActive(grilleObject))
        {
            currentPebbleID = grilleObject.GetComponentInChildren<PebbleController>().PebbleColorID;
        }
    }

    #region Pebbles references methods
    private Sprite GetPebbleSprite(int columnNumber, int rowNumber)
    {

        switch (rowNumber)
        {
            case 1:
                if (firstRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return firstRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 2:
                if (secondRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return secondRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 3:
                if (thirdRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return thirdRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 4:
                if (fourthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return fourthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 5:
                if (fifthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return fifthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 6:
                if (sixthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return sixthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 7:
                if (seventhRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return seventhRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 8:
                if (eightRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return eightRowGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;
            case 9:
                if (ninthColumnGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return ninthColumnGrills[columnNumber].GetComponentInChildren<PebbleController>().GetComponent<SpriteRenderer>().sprite;
                else return null;

            default:
                return null;
        }
    }


    private int GetPebbleID(int columnNumber, int rowNumber)
    {
        switch (rowNumber)
        {
            case 1:
                if (firstRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return firstRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 2:
                if (secondRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return secondRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 3:
                if (thirdRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return thirdRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 4:
                if (fourthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return fourthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 5:
                if (fifthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return fifthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 6:
                if (sixthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return sixthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 7:
                if (seventhRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return seventhRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 8:
                if (eightRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return eightRowGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            case 9:
                if (ninthColumnGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                    return ninthColumnGrills[columnNumber].GetComponentInChildren<PebbleController>().PebbleColorID;
                else return 0;
            default:
                return 0;
        }
    }

    private GameObject GetPebbleFromCoordinates(int columnNumber, int rowNumber)
    {
        switch (rowNumber)
        {
            case 1:
                if (firstRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return firstRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 2:
                if (secondRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return secondRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 3:
                if (thirdRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return thirdRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 4:
                if (fourthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return fourthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 5:
                if (fifthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return fifthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 6:
                if (sixthRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return sixthRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 7:
                if (seventhRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return seventhRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 8:
                if (eightRowGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return eightRowGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            case 9:
                if (ninthColumnGrills[columnNumber].GetComponentInChildren<PebbleController>() != null)
                {
                    return ninthColumnGrills[columnNumber].GetComponentInChildren<PebbleController>().gameObject;
                }
                else
                    return null;
            default:
                return null;
        }
    }
    #endregion

    private void IncrementPoints()
    {
        if (!isFirstCheck)
        {
            
            points += POINTS_FOR_MATCHES;
            pointsText.text = "POINTS:" + points;
            if (points > PlayerPrefs.GetFloat(BEST_SCORE_PREFS))
            {
                PlayerPrefs.SetFloat(BEST_SCORE_PREFS, points);
                bestScoreText.text = "BEST SCORE:" + points;
            }
        }
    }


    #region Bool conditions

    private bool LastPebbleInRowIsAMatch(List<GameObject> currentRow)
    {
        return currentPebbleID == currentRow[currentIndexHorizontal - 1].GetComponentInChildren<PebbleController>().PebbleColorID && colorCounterHorizontal >= 2;
    }

    private bool LastPebbleInRowCurrentlyActive(List<GameObject> currentRow)
    {
        return currentIndexHorizontal > 0 && currentRow[currentIndexHorizontal - 1] != null && currentRow[currentIndexHorizontal - 1].GetComponentInChildren<PebbleController>() != null;
    }

    private bool PebbleCurrentlyActive(GameObject referenceObject)
    {
        return referenceObject.GetComponentInChildren<PebbleController>() != null;
    }

    private bool PebbleHasRightNeighbour(int currentIndex, List<GameObject> currentRow)
    {
        return currentIndex != 8 && PebbleCurrentlyActive(currentRow[currentIndex + 1]);
    }
    private bool PebbleRightNeighbourHasSameColor(int currentIndex, List<GameObject> currentRow)
    {
        return currentPebbleID == currentRow[currentIndex + 1].GetComponentInChildren<PebbleController>().PebbleColorID;
    }
    #endregion

}
