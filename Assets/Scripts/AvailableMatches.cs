﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvailableMatches : MonoBehaviour
{
    int firstPairColumn = -1;
    int firstPairRow = -1;
    private bool matchPossible = false;

    [SerializeField]
    private List<GameObject> allGrills;

    [SerializeField]
    private List<Sprite> pebbleSprites;

    bool horizontalMatchPossible;
    bool horizontalMatchFromGapPairPossible;

    MatchesController matchesInstance;
    GameObject firstPairObject;

    void Awake()
    {
        matchesInstance = FindObjectOfType<MatchesController>();
    }

    void OnEnable()
    {

        MatchesController.OnPebblesChange += CheckAvailableMatches;

    }


    void OnDestroy()
    {
        MatchesController.OnPebblesChange -= CheckAvailableMatches;

    }


    public void CheckAvailableMatches()
    {
        horizontalMatchPossible = false;
        StartCoroutine(FindPairsWithDelay());

    }

    IEnumerator FindPairsWithDelay()
    {
        yield return new WaitForSeconds(0.5f);

        FindPairsMatches();
        if (!horizontalMatchPossible)
        {
            ReshufflePebbles();
        }
    }

    #region Finding matches
    bool FindPairsMatches()
    {

        bool pairMatchFound = false;

        if (IsPairInRow(matchesInstance.FirstRowGrills) != -1)
        {
            firstPairRow = 1;
            firstPairColumn = IsPairInRow(matchesInstance.FirstRowGrills);

            if (IsHorizontalMatchPossible())
            {
                pairMatchFound = true;
            }

        }

        if (!pairMatchFound)
        {

            if (IsPairInRow(matchesInstance.SecondRowGrills) != -1)
            {
                firstPairRow = 2;
                firstPairColumn = IsPairInRow(matchesInstance.SecondRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;

                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairInRow(matchesInstance.ThirdRowGrills) != -1)
            {
                firstPairRow = 3;
                firstPairColumn = IsPairInRow(matchesInstance.ThirdRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairInRow(matchesInstance.FourthRowGrills) != -1)
            {
                firstPairRow = 4;
                firstPairColumn = IsPairInRow(matchesInstance.FourthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairInRow(matchesInstance.FifthRowGrills) != -1)
            {
                firstPairRow = 5;
                firstPairColumn = IsPairInRow(matchesInstance.FifthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {

            if (IsPairInRow(matchesInstance.SixthRowGrills) != -1)
            {
                firstPairRow = 6;
                firstPairColumn = IsPairInRow(matchesInstance.SixthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairInRow(matchesInstance.SeventhRowGrills) != -1)
            {
                firstPairRow = 7;
                firstPairColumn = IsPairInRow(matchesInstance.SeventhRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairInRow(matchesInstance.EightRowGrills) != -1)
            {
                firstPairRow = 8;
                firstPairColumn = IsPairInRow(matchesInstance.EightRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {

            if (IsPairInRow(matchesInstance.NinthRowGrills) != -1)
            {
                firstPairRow = 9;
                firstPairColumn = IsPairInRow(matchesInstance.NinthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.FirstRowGrills) != -1)
            {

                firstPairRow = 1;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.FirstRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.SecondRowGrills) != -1)
            {

                firstPairRow = 2;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.SecondRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.ThirdRowGrills) != -1)
            {

                firstPairRow = 3;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.ThirdRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.FourthRowGrills) != -1)
            {

                firstPairRow = 4;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.FourthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.FifthRowGrills) != -1)
            {

                firstPairRow = 5;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.FifthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.SixthRowGrills) != -1)
            {

                firstPairRow = 6;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.SixthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.SeventhRowGrills) != -1)
            {

                firstPairRow = 7;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.SeventhRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.EightRowGrills) != -1)
            {

                firstPairRow = 8;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.EightRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }

        if (!pairMatchFound)
        {
            if (IsPairWithGapInRow(matchesInstance.NinthRowGrills) != -1)
            {

                firstPairRow = 9;
                firstPairColumn = IsPairWithGapInRow(matchesInstance.NinthRowGrills);

                if (IsHorizontalMatchPossible())
                {
                    pairMatchFound = true;
                }
            }
        }



        return pairMatchFound;
    }



    int IsPairInRow(List<GameObject> GrillsRow)
    {
        int currentColumn = 0;
        int columnWithPair = -1;

        foreach (GameObject grill in GrillsRow)
        {

            if (currentColumn < 8 && grill.GetComponentInChildren<PebbleController>() != null)
            {
                GameObject pebble = grill.GetComponentInChildren<PebbleController>().gameObject;

                if (grill.GetComponentInChildren<PebbleController>() != null && GrillsRow[currentColumn + 1].GetComponentInChildren<PebbleController>() != null)
                {
                    if (grill.GetComponentInChildren<PebbleController>().PebbleColorID == GrillsRow[currentColumn + 1].GetComponentInChildren<PebbleController>().PebbleColorID)
                    {
                        columnWithPair = currentColumn;
                        firstPairObject = grill;
                    }
                }
            }
            currentColumn++;
        }
        return columnWithPair;
    }

    int IsPairWithGapInRow(List<GameObject> GrillsRow)
    {
        int currentColumn = 0;
        int columnWithPair = -1;
        int gapCounter = 0;

        foreach (GameObject grill in GrillsRow)
        {

            if (currentColumn < 8 && grill.GetComponentInChildren<PebbleController>() != null)
            {
                GameObject pebble = grill.GetComponentInChildren<PebbleController>().gameObject;

                if (grill.GetComponentInChildren<PebbleController>() != null && GrillsRow[currentColumn + 1].GetComponentInChildren<PebbleController>() != null)
                {
                    if (grill.GetComponentInChildren<PebbleController>().PebbleColorID == GrillsRow[currentColumn + 1].GetComponentInChildren<PebbleController>().PebbleColorID)
                    {
                        columnWithPair = currentColumn;
                        firstPairObject = grill;
                    }
                    else
                    {
                        if (currentColumn < 7)
                        {
                            if (grill.GetComponentInChildren<PebbleController>().PebbleColorID == GrillsRow[currentColumn + 2].GetComponentInChildren<PebbleController>().PebbleColorID)
                            {
                                gapCounter++;
                            }
                        }
                    }
                }
            }
            currentColumn++;
        }
        if (gapCounter == 1)
            return columnWithPair;
        else
            return -1;
    }

    bool IsHorizontalMatchPossible()
    {
        GameObject pebbleInGrill = firstPairObject.GetComponentInChildren<SingleGrilleController>().gameObject;
        int pebbleInGrillID = pebbleInGrill.GetComponent<SingleGrilleController>().CurrentColorID;

        if (firstPairObject.GetComponent<SingleGrilleController>().possibleMatchesLeft != null)
        {
            foreach (GameObject possibleMatch in firstPairObject.GetComponent<SingleGrilleController>().possibleMatchesLeft)
            {
                if (possibleMatch.GetComponent<SingleGrilleController>().CurrentColorID == pebbleInGrillID)
                {

                    horizontalMatchPossible = true;
                }
            }
        }

        else if (firstPairObject.GetComponent<SingleGrilleController>().possibleMatchesRight != null)
        {
            foreach (GameObject possibleMatch in firstPairObject.GetComponent<SingleGrilleController>().possibleMatchesRight)
            {
                if (possibleMatch.GetComponent<SingleGrilleController>().CurrentColorID == pebbleInGrillID)
                {

                    horizontalMatchPossible = true;
                }
            }
        }
        return horizontalMatchPossible;
    }

    bool IsMatchFromPairWithGapPossible()
    {
        GameObject pebbleInGrill = firstPairObject.GetComponentInChildren<SingleGrilleController>().gameObject;
        int pebbleInGrillID = pebbleInGrill.GetComponent<SingleGrilleController>().CurrentColorID;

        if (firstPairObject.GetComponent<SingleGrilleController>().possibleMatchesMiddle != null)
        {
            foreach (GameObject possibleMatch in firstPairObject.GetComponent<SingleGrilleController>().possibleMatchesMiddle)
            {
                if (possibleMatch.GetComponent<SingleGrilleController>().CurrentColorID == pebbleInGrillID)
                {

                    horizontalMatchFromGapPairPossible = true;
                }
            }
        }

        return horizontalMatchFromGapPairPossible;
    }
    #endregion

    void ReshufflePebbles()
    {
        Debug.Log("RESHUFFLING!");
        List<int> allPebblesID = new List<int>();
        foreach (GameObject grille in allGrills)
        {
            int pebbleID = grille.GetComponentInChildren<SingleGrilleController>().CurrentColorID;
            allPebblesID.Add(pebbleID);
        }


        allPebblesID = Randomize(allPebblesID);
        int grilleIndex = 0;

        foreach (GameObject grille in allGrills)
        {
            int currentRandomIndex = allPebblesID[grilleIndex];
            grille.GetComponentInChildren<PebbleController>().ChangePebbleColor(pebbleSprites[currentRandomIndex], currentRandomIndex);
            grille.GetComponent<SingleGrilleController>().CurrentColorID = currentRandomIndex;
            grilleIndex++;
        }
        matchesInstance.CheckMatches();

    }

    private List<T> Randomize<T>(List<T> list)
    {
        List<T> randomizedList = new List<T>();

        while (list.Count > 0)
        {
            int index = Random.Range(0, list.Count);
            randomizedList.Add(list[index]);
            list.RemoveAt(index);
        }
        return randomizedList;
    }

}
