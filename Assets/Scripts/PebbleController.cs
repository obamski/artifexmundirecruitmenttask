﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PebbleController : MonoBehaviour {
    [SerializeField] //TODO; hide after tests
    private int pebbleColorID;
    [SerializeField]
    private GameObject parentGrille;

    public int PebbleColorID
    {
        get { return pebbleColorID; }
        set { pebbleColorID = value; }
    }

    public void ChangePebbleColor(Sprite newColor, int newId)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = newColor;
        pebbleColorID = newId;
    }
}
