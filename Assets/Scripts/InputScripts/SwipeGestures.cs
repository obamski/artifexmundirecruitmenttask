﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeGestures : MonoBehaviour
{


    private bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
    private bool isDraging;
    private Vector2 startTouch, swipeDelta;
    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeLeft { get { return swipeLeft; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeDown { get { return swipeDown; } }
    public bool SwipeUp { get { return swipeUp; } }
    private const float SWIPE_DETECTION_MAGNITUDE = 40 ;
    [SerializeField]
    private Text timerText;

    private float timer = 0;

    string colliderName;

    private void Update()
    {
        tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;

        #region Standalone Input
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            isDraging = true;
            startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            ResetStartTouch();
        }
        #endregion
        #region Mobile Inputs
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                isDraging = true;
                tap = true;

                startTouch = Input.touches[0].position;

            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDraging = false;
                ResetStartTouch();
            }
        }

        #endregion

        swipeDelta = Vector2.zero;
        if (isDraging)
        {
            if (Input.touches.Length > 0)
                swipeDelta = Input.touches[0].position - startTouch;
            else if (Input.GetMouseButton(0))
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
        }

        if (swipeDelta.magnitude > SWIPE_DETECTION_MAGNITUDE)
        {
            float x = swipeDelta.x;
            float y = swipeDelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                {
                    swipeLeft = true;
                    CheckHitColliders((int)Swipes.LEFT);
                }
                else
                {
                    swipeRight = true;
                    CheckHitColliders((int)Swipes.RIGHT);
                }
            }
            else
            {
                if (y < 0)
                {
                    swipeDown = true;
                    CheckHitColliders((int)Swipes.DOWN);

                }
                else
                {
                    swipeUp = true;
                    CheckHitColliders((int)Swipes.UP);
                }
            }
            ResetStartTouch();
        }

    }

    private void CheckHitColliders(int swipeID)
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(startTouch), Vector2.zero);

        if (hit.collider != null)
        {

            hit.collider.gameObject.GetComponentInParent<SingleGrilleController>().PebbleSwipe(swipeID);

        }
    }


    private void ResetStartTouch()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }

   
}
